﻿using System.Net;

namespace Adp.Labs.Requests.Api.Models
{
    public sealed class CalculationResponse
    {
        public string? Description { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
