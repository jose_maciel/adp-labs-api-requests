﻿using Adp.Labs.Requests.Api.Enums;

namespace Adp.Labs.Requests.Api.Models
{
    public sealed class Calculation
    {
        public string? Id { get; set; }
        public OperationsEnum? Operation { get; set; }
        public long Left { get; set; }
        public long Right { get; set; }
    }
}
