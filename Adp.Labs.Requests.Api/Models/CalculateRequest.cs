﻿namespace Adp.Labs.Requests.Api.Models
{
    public sealed class CalculateRequest
    {
        public string Id { get; set; }
        public long Result { get; set; }
    }
}
