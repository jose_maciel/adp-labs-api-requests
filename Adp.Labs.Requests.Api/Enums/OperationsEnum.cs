﻿namespace Adp.Labs.Requests.Api.Enums
{
    public enum OperationsEnum
    {
        Subtraction,
        Addition,
        Remainder,
        Multiplication,
        Division
    }
}
