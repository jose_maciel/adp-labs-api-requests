using Adp.Labs.Requests.Api.Models;
using Adp.Labs.Requests.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Adp.Labs.Requests.Api.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v1/[controller]")]
    public class CalculationsController : ControllerBase
    {
        private readonly ICalculationService _calculationService;

        public CalculationsController(ICalculationService calculationService)
        {
            _calculationService = calculationService;
        }

        [HttpGet()]
        public async Task<IActionResult> Get()
        {
            var operation = await _calculationService.GetCalculationAsync();

            var postRequest = new CalculateRequest
            {
                Id = operation.Id!,
                Result = _calculationService.Calculate(operation)
            };

            var result = await _calculationService.PostCalculationAsync(postRequest);

            return StatusCode((int)result.StatusCode, result.Description);
        }
    }
}
