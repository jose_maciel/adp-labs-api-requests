﻿using Adp.Labs.Requests.Api.Configs;
using Adp.Labs.Requests.Api.Interfaces.Refit;
using Adp.Labs.Requests.Api.Services;
using Adp.Labs.Requests.Api.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Polly;
using Polly.Extensions.Http;
using Refit;
using System.Diagnostics.CodeAnalysis;
using System.Net;

namespace Adp.Labs.Requests.Api.Extensions
{
    [ExcludeFromCodeCoverage]
    public static class ServiceCollectionExtensions
    {        
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<ICalculationService, CalculationService>();

            return services;
        }

        public static IServiceCollection AddRefitInterfaces(this IServiceCollection services, ConfigurationManager configuration)
        {
            services.AddRefitClient<IinterviewExternalApi>().ConfigureHttpClient(c =>
            {
                var apiUrl = configuration.GetSection(ApiUrlsOptions.InterviewUrl).Get<string>();
                c.BaseAddress = new Uri(apiUrl);
            })
            .AddHttpMessageHandler(provider =>
            {
                var retryPolicy = HttpPolicyExtensions
                    .HandleTransientHttpError()
                    .OrResult(response => response.StatusCode ==  HttpStatusCode.ServiceUnavailable)
                    .WaitAndRetryAsync(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));

                return new PolicyHandler(retryPolicy);
            });

            return services;
        }

        public static IServiceCollection AddCustomSwagger(this IServiceCollection services)
        {
            return services.AddSwaggerGen(c => c.SwaggerDoc("v1", new OpenApiInfo { Title = "Adp Labs Interview Api", Version = "v1" }));
        }

        public static IServiceCollection AddApplicationApiVersioning(this IServiceCollection services)
        {
            return services.AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ReportApiVersions = true;
            });
        }
    }
}
