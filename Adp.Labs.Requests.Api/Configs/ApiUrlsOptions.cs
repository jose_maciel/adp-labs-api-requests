﻿namespace Adp.Labs.Requests.Api.Configs
{
    public sealed class ApiUrlsOptions
    {
        public const string InterviewUrl = "InterviewApiUrl";

        public string InterviewApiUrl { get; set; } = default!;
    }
}
