﻿using Adp.Labs.Requests.Api.Models;
using Refit;

namespace Adp.Labs.Requests.Api.Interfaces.Refit
{
    public interface IinterviewExternalApi
    {
        [Get("/get-task")]
        Task<Calculation> Get();

        [Post("/submit-task")]
        Task<HttpResponseMessage> Post([Body]CalculateRequest calculateRequest);
    }
}
