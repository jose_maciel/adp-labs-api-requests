﻿using Polly;
using System.Diagnostics.CodeAnalysis;

namespace Adp.Labs.Requests.Api.Utils
{
    [ExcludeFromCodeCoverage]
    public class PolicyHandler : DelegatingHandler
    {
        private readonly IAsyncPolicy<HttpResponseMessage> _policy;

        public PolicyHandler(IAsyncPolicy<HttpResponseMessage> policy)
        {
            _policy = policy ?? throw new ArgumentNullException(nameof(policy));
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return _policy.ExecuteAsync(() => base.SendAsync(request, cancellationToken));
        }
    }
}
