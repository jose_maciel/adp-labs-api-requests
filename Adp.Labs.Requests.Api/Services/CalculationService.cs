﻿using Adp.Labs.Requests.Api.Enums;
using Adp.Labs.Requests.Api.Interfaces.Refit;
using Adp.Labs.Requests.Api.Models;
using System.Text.Json;

namespace Adp.Labs.Requests.Api.Services
{
    public class CalculationService : ICalculationService
    {
        private readonly IinterviewExternalApi _interviewExternalApi;
        private readonly ILogger<CalculationService> _logger;

        public CalculationService(IinterviewExternalApi iinterviewExternalApi, ILogger<CalculationService> logger)
        {
            _interviewExternalApi = iinterviewExternalApi;
            _logger = logger;
        }

        public async Task<Calculation> GetCalculationAsync()
        {
            try
            {
                _logger.LogInformation("{method} - starting request", nameof(GetCalculationAsync));
                var calculation = await _interviewExternalApi.Get();
                _logger.LogInformation("{method} - get following operation from api: {response}", nameof(GetCalculationAsync), JsonSerializer.Serialize(calculation));

                return calculation;
            }
            catch (Exception ex)
            {
                _logger.LogError("{method} - error while processing request {error}", nameof(GetCalculationAsync), ex.Message);

                throw;
            }
        }

        public async Task<CalculationResponse> PostCalculationAsync(CalculateRequest calculateRequest)
        {
            try
            {
                _logger.LogInformation("{method} - starting request", nameof(PostCalculationAsync));
                var validationResult = await _interviewExternalApi.Post(calculateRequest);
                var response = new CalculationResponse { Description = await validationResult.Content.ReadAsStringAsync(), StatusCode = validationResult.StatusCode };
                _logger.LogInformation("{method} - got following response from api: {response}", nameof(PostCalculationAsync), JsonSerializer.Serialize(response));

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError("{method} - error while processing request {error}", nameof(PostCalculationAsync), ex.Message);

                throw;
            }

        }

        public long Calculate(Calculation calculation)
        {
            return calculation.Operation switch
            {
                OperationsEnum.Addition => calculation.Left + calculation.Right,
                OperationsEnum.Subtraction => calculation.Left - calculation.Right,
                OperationsEnum.Multiplication => calculation.Left * calculation.Right,
                OperationsEnum.Division => calculation.Left / calculation.Right,
                OperationsEnum.Remainder => calculation.Left % calculation.Right,
                _ => throw new InvalidOperationException("Invalid operation was requested"),
            };
        }
    }
}
