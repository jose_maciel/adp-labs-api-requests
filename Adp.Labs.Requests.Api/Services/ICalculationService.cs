﻿using Adp.Labs.Requests.Api.Models;

namespace Adp.Labs.Requests.Api.Services
{
    public interface ICalculationService
    {
        Task<Calculation> GetCalculationAsync();
        Task<CalculationResponse> PostCalculationAsync(CalculateRequest calculateRequest);
        long Calculate(Calculation calculation);
    }
}
