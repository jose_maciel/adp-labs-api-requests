using Adp.Labs.Requests.Api.Configs;
using Adp.Labs.Requests.Api.Extensions;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.Configure<ApiUrlsOptions>
(
    builder.Configuration.GetSection(ApiUrlsOptions.InterviewUrl)
);
builder.Services.AddApplicationServices();
builder.Services.AddApplicationApiVersioning();
builder.Services.AddRefitInterfaces(builder.Configuration);
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddCustomSwagger();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
