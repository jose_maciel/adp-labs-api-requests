﻿using Adp.Labs.Requests.Api.Enums;
using Adp.Labs.Requests.Api.Interfaces.Refit;
using Adp.Labs.Requests.Api.Models;
using Adp.Labs.Requests.Api.Services;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System.Net;

namespace Adp.Labs.Requests.Api.Tests.Services
{
    [TestClass]
    public class CalculationServiceTests
    {
        private readonly IinterviewExternalApi _interviewExternalApi;
        private readonly ILogger<CalculationService> _logger;

        public CalculationServiceTests()
        {
            _interviewExternalApi = Substitute.For<IinterviewExternalApi>();
            _logger = Substitute.For<ILogger<CalculationService>>();
        }

        [TestMethod]
        public async Task GetCalculationShouldReturnCalculationFromApi()
        {
            // Arrange
            var calculationService = new CalculationService(_interviewExternalApi, _logger);
            var expectedCalculation = new Calculation { Left = 5, Right = 3, Operation = OperationsEnum.Addition };
            _interviewExternalApi.Get().Returns(Task.FromResult(expectedCalculation));

            // Act
            var result = await calculationService.GetCalculationAsync();

            // Assert
            result.Should().BeEquivalentTo(expectedCalculation);
        }

        [TestMethod]
        public async Task PostCalculationShouldReturnCaltulationResponseFromApi()
        {
            // Arrange
            var calculationService = new CalculationService(_interviewExternalApi, _logger);
            var calculateRequest = new CalculateRequest { Id = "0123-124234-123-423", Result = 8 };
            var validationResult = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("Correct")
            };

            _interviewExternalApi.Post(calculateRequest).Returns(Task.FromResult(validationResult));

            // Act
            var result = await calculationService.PostCalculationAsync(calculateRequest);

            // Assert
            result.Should().BeEquivalentTo(new CalculationResponse
            {
                StatusCode = HttpStatusCode.OK,
                Description = "Correct"
            });
        }

        [TestMethod]
        public void CalculateShouldPerformCorrectOperation()
        {
            // Arrange
            var calculationService = new CalculationService(_interviewExternalApi, _logger);

            var calculation = new Calculation { Left = 5, Right = 3, Operation = OperationsEnum.Addition };

            // Act
            var result = calculationService.Calculate(calculation);

            // Assert
            result.Should().Be(8);
        }

        [TestMethod]
        public void CalculateShouldThrowExceptionForInvalidOperation()
        {
            // Arrange
            var calculationService = new CalculationService(_interviewExternalApi, _logger);

            var calculation = new Calculation { Left = 5, Right = 3, Operation = null };

            // Act
            Action act = () => calculationService.Calculate(calculation);

            // Assert
            act.Should().Throw<InvalidOperationException>().WithMessage("Invalid operation was requested");
        }
    }
}
