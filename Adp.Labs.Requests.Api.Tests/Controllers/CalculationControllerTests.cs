using Adp.Labs.Requests.Api.Controllers;
using Adp.Labs.Requests.Api.Enums;
using Adp.Labs.Requests.Api.Models;
using Adp.Labs.Requests.Api.Services;
using FluentAssertions;
using FluentAssertions.Execution;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using System.Net;

namespace Adp.Labs.Requests.Api.Tests
{
    [TestClass]
    public class CalculationControllerTests
    {
        [TestMethod]
        public async Task GetShouldReturnStatusCodeFromPostCalculation()
        {
            // Arrange
            var calculationService = Substitute.For<ICalculationService>();
            var controller = new CalculationsController(calculationService);

            var calculationResult = new Calculation
            {
                Id = "0123-124234-123-423",
                Left = 5,
                Right = 3,
                Operation = OperationsEnum.Addition
            };

            calculationService.GetCalculationAsync().Returns(Task.FromResult(calculationResult));

            var expectedResult = new CalculationResponse
            {
                StatusCode = HttpStatusCode.OK,
                Description = "Calculation successful."
            };

            calculationService.PostCalculationAsync(Arg.Any<CalculateRequest>()).Returns(Task.FromResult(expectedResult));

            // Act
            var actionResult = await controller.Get();

            // Assert
            using (new AssertionScope())
            {
                var statusCodeResult = actionResult.Should().BeOfType<ObjectResult>().Subject;
                statusCodeResult.StatusCode.Should().Be((int)HttpStatusCode.OK);
                var content = statusCodeResult.Value.Should().BeAssignableTo<string>().Subject;
                content.Should().Be(expectedResult.Description);
                await calculationService.Received(1).GetCalculationAsync();
                await calculationService.Received(1).PostCalculationAsync(Arg.Any<CalculateRequest>());
            }
        }
    }
}
